(ns slack-admin.handler
  (:require [compojure.core :refer :all]
            [compojure.handler :refer [site]]
            [compojure.route :as route]
            [clojure.data.json :as json]
            [clojure.core.async :refer [thread]]
            [clj-http.client :as client]
            [ring.adapter.jetty :as jetty]
            [ring.middleware.defaults :refer [wrap-defaults api-defaults]]
            [environ.core :refer [env]]))

(def auth-token (get (System/getenv) "SLACK_AUTH_TOKEN" "butts"))
(def hook-url (get (System/getenv) "SLACK_HOOK_URL" "butts"))

(defn post-to-slack
  [url msg]
  (let [m (merge {:username "Admin"
                  :icon_emoji ":mega:"} msg)]
    (client/post url {:body (json/write-str msg)
                      :content-type :json})))

(defn format-user
  [user_id]
  (str "<@" user_id ">"))

(defn format-channel
  [channel_id]
  (str "<#" channel_id ">"))

(defn format-admin-message
  [channel user message]
  (str (format-user user) " ["
       (format-channel channel) "]: "
       message))

(defn admin-to-slack
  [message user channel]
  (post-to-slack hook-url {:text (format-admin-message channel user message)
                           :icon_emoji ":mega:"
                           :channel "#admin"}))

(defroutes app-routes
  (POST "/slack" {:keys [params] :as request}
        (if (and (= "/admin" (:command params))
                 (= auth-token (:token params)))
          (do
            (thread (admin-to-slack (clojure.string/trim (:text params))
                                    (:user_id params)
                                    (:channel_id params)))
            {:status 200
             :content-type "text/plain"
             :body "Your message has been sent to the admins."})))
  (route/resources "/")
  (route/not-found "Not Found"))

(def app
  (wrap-defaults app-routes api-defaults))

(defn -main [& [port]]
  (let [port (Integer. (or port (env :port) 5000))]
    (jetty/run-jetty (site #'app) {:port port :join? false})))
